find_package(CUDA REQUIRED)

if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
  set(CUDA_SAMPLES_DIR "C:/ProgramData/NVIDIA Corporation/CUDA Samples")
  set(CUDA_SAMPLES_DIR "${CUDA_SAMPLES_DIR}/v${CUDA_VERSION_STRING}")
else(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
	file(GLOB CUDA_LINUX "/usr/local/cuda*")
  find_path(CUDA_SAMPLES_DIR "common/inc/helper_cuda.h"
  	PATHS ${CUDA_TOOLKIT_ROOT_DIR} ${CUDA_LINUX}
  	PATH_SUFFIXES "samples")
endif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")

if(EXISTS ${CUDA_SAMPLES_DIR})
  message("-- Found CUDA_samples: ${CUDA_SAMPLES_DIR}")
else(EXISTS ${CUDA_SAMPLES_DIR})
  message("-- CUDA_samples not found: ${CUDA_SAMPLES_DIR}")
endif(EXISTS ${CUDA_SAMPLES_DIR})
