#pragma once

#ifdef PLATFORM_OSX
#include <cutil_inline.h>
#include <sdkHelper.h>
#else // Linux, Windows
#include <cuda_runtime.h>
#include <helper_cuda.h>	// includes cuda.h and cuda_runtime_api.h
#include <helper_timer.h>
#include <helper_functions.h>
#endif
