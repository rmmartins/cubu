#pragma once 

//Implementation header: Should only be used by implementation-level code, not client code
//
//

#include "include/cpubundling.h"
#include <cuda_runtime.h>
#include <curand_kernel.h>

//!!!Probably move all this to GPUBundling class decl




typedef float4 BundlingPoint;									//An edge sample-point, with additional data for bundling


const int	NTHREADS_X = 32;									//Max #threads we can spawn on the current GPU (x direction)
const int	NTHREADS_Y = 16;									//Max #threads we can spawn on the current GPU (y direction)	
const int	NTHREADS   = NTHREADS_X*NTHREADS_Y;					//Max #threads we can spawn on the current GPU (in a thread-block)

const int	EDGE_PROFILE_SIZE = 1024;
const int	MAX_KERNEL_LENGTH = 2*128+1;						//Maximum kernel length being ever used (must be an odd number)



extern "C" void setConvolutionKernel(float* h_Kernel,int kernel_size,int kernel_radius);

extern "C" void random_init(curandState* d_states);

extern "C" void initializeSiteLocations(CPUBundling::DENSITY_ESTIM,cudaArray* a_input,float* d_outSitemap,unsigned int* d_Count,BundlingPoint* d_inpoints,int npoints,float value,int imageW,int imageH);

extern "C" void convolutionGPU(float* d_outDensity,cudaArray* a_inpSitemap,int imageW,int imageH);

extern "C" void advectSites(BundlingPoint* out_points,BundlingPoint* in_points,int npoints,cudaArray* a_inpDensity,int imageW,int imageH,float h);						

extern "C" void resample(BundlingPoint* outpoints,int& n_outpoints,int* outedges,BundlingPoint* in_points,int n_inpoints,int* in_edges,int* h_edges,int n_edges,float delta,curandState* d_rndstates,float jitter,
						 float* d_edgeProfile);

extern "C" void smoothLines(BundlingPoint* out_points,BundlingPoint* in_points,int npoints,float t,float h,float filter_kernel,int niter);


