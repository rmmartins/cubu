#pragma once

//  GraphDrawing:					Encodes the 2D drawing of a graph. The drawing doesn't store an explicit reference back to the graph.
//									Instead, we build the drawing from a graph (given as a Graph), and next use the drawing.
//									A drawing is encoded as a set of curves (polylines).  
//
//
//


#include "include/point2d.h"
#include <vector>

#ifndef __CUDACC__
#ifdef PLATFORM_WIN
#include <unordered_map>
#else
#include <tr1/unordered_map>
#endif
#endif

#include <map>


class Graph;



class GraphDrawing
{
public:

enum COLOR_MODE			//Types of color mapping used for edges	
	 { 
		GRAYSCALE = 0,
		RAINBOW,
		INVERSE_RAINBOW
	 };

enum ALPHA_MODE			//Types of transparency mapping used for edges	
	 { 
		ALPHA_CONSTANT = 0,
		ALPHA_VALUE,
		ALPHA_INVERSE_VALUE
	 };

struct Polyline : public std::vector<Point2d>
{
			Polyline(float v=0,int res=2): value(v) { reserve(res); }
void		interpolate(const Polyline&,float t);			
float		length() const;
float		value;			
};


#ifndef __CUDACC__
typedef std::tr1::unordered_map<int,Polyline*>	Row;
#endif
typedef std::multimap<float,const Polyline*>	DrawingOrder;


						GraphDrawing();
virtual				   ~GraphDrawing();	
const GraphDrawing&		operator=(const GraphDrawing& rhs);
void					build(const Graph*,const PointSet*);
void					resample(float delta,float jitter=0);
#ifndef __CUDACC__
const Row&				operator()(int i) const { return drawing[i]; }
Row&					operator()(int i)		{ return drawing[i]; }
int						numNodes() const		{ return drawing.size(); }
#endif
int						numEdges() const		{ return num_edges; }
virtual void			draw() const;
void					normalize(const Point2d& dim,float border);
void					saveTrails(const char* fn) const;
bool					readTrails(const char* fn);
void					interpolate(const GraphDrawing&,float t);

void					resample(Polyline&,float delta,float jitter);

#ifndef __CUDACC__
std::vector<Row>		drawing;			//edges from i-th point to all points > i
#endif
DrawingOrder			draw_order;			//edges sorted by some (drawing) order
float					val_min,val_max;	//range of edge values
bool					draw_points;		//whether to draw control points	
bool					draw_edges;			//whether to draw edges (polylines)
bool					draw_endpoints;		//whether to draw edge endpoints
float					line_width;			//line width in drawing
int						num_edges;
float					global_alpha;		//Global transparency factor
bool					global_blend;		//Global blending on/off
COLOR_MODE				color_mode;			//Colormap used for edges
ALPHA_MODE				alpha_mode;			//Alpha map used for edges
int						densityMapSize;		//Density map size (see below)
float*					densityMap;			//Density map (for drawing, not owned); Either 0 (not initialized), or initialized to a densityMapSize^2 array
bool					use_density_alpha;	//Use density map to modulate global alpha

protected:

void					float2alpha(float value,float& alpha) const;
void					float2rgb(float value,float& R,float& G,float& B) const;
};


