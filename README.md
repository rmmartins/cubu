# CUBu

Cuda-based Bundling.

### Notes for Windows Users

* There are compatibility problems between some CUDA toolkit and Microsoft Visual Studio versions, so check [here](http://docs.nvidia.com/cuda/cuda-getting-started-guide-for-microsoft-windows/index.html#system-requirements) before you start.

    * Make sure you have a version of MSVS that supports building x86_32 applications; I'm not sure we can successfully build on x86_64 right now.

* It's easy for CMAKE to find libraries/headers on Linux because they're (almost) always in the default paths, such as /usr or /usr/local. On Windows, however, they could be anywhere, which makes it harder for CMAKE to find them. So, when installing dependencies, follow these instructions (restart CMAKE-GUI if you change any environment variable while it's open):
	
    1. Put all .dll's on C:\Windows\System32 (for 32-bit system) or SysWOW64 (for 64-bit system)
    2. Put all .lib's in a folder and point to it with the $LIB environment variable.
    3. Put all .h's in a folder and point to it with the $INCLUDE environment variable.
		
* If you're on a 64-bit system, you should build **CUBu** as a 32-bit application. The reason is that many of the dependencies only offer 32-bit pre-compiled binaries and some won't even build correctly on Windows 64-bit. There's no downside to building **CUBu** in 32-bit.
	
### Dependencies

1. [FreeGLUT](http://freeglut.sourceforge.net/) -- Last tested version: 2.8.1 (10/08/2014)
    * Win32: There are pre-compiled binaries [here](http://www.transmissionzero.co.uk/software/freeglut-devel/).
    * Unix: just get it from your packaging system (ex.: freeglut3-dev).
		
1. [GLUI](http://glui.sourceforge.net/) -- Last tested version: 2.35 (10/08/2014)
	* Win32: Download the sources and build the (ancient) solution found in src/msvc. 
		* It depends on (Free)GLUT so add $(INCLUDE) to the project's include dirs. 
		* Change the Target Name (on Properties/General) to "glui32" and the Output File (under Properties/Librarian) to "Debug\glui32.lib". 

		* This will not generate a DLL. Install .lib and .h as defined previously.
	* Unix: just get it from your packaging system (ex.: libglui-dev). On Ubuntu it has been removed from recent repos, but you can find it [here](http://packages.ubuntu.com/raring/libglui-dev), for example.
	
1. [CUDA](https://developer.nvidia.com/cuda-downloads) -- Last tested version: 6.0.37 (10/08/2014)
	* All platforms: just install the correct package and CMAKE should be able to find it properly (no need to install files as described previously).
	* The code is currently using the following helper-headers from the CUDA samples package. They're usually packaged with the official toolkit; if you have an Ubuntu installation that doesn't include samples, you can also download them around the web (it doesn't really matter).
		* exception.h
		* helper_cuda.h
		* helper_functions.h
		* helper_image.h
		* helper_string.h
		* helper_timer.h

### Build ###

1. The preferred way is to build with CMAKE. For example:
    * Win32:
		# Create a new "build_win32" folder for the binaries.
		# Run CMAKE-GUI, point "source code" to the base dir, and "binaries" to your new "build_win32" folder.
		# Configure and choose your VS version, but make sure you DON'T choose the "win64" version. 
		# Generate. This will create a VS solution (CUBu.sln) that you can (hopefully) build without problem. Building the INSTALL project will put the executables in the right place (root of the solution).
    * Unix: mkdir -p build && cd build && cmake .. && make install

2. If you don't want to / can't use CMAKE, try Makefile.old at your own risk. A simple "make" should work. If it doesn't, check Makefile.old to see if everything is correct. There are specific options for OSX and Linux. Pay attention specially to the CUDAGEN variable, which stores parameters for CUDA architecture.

### Running ###

Run with "./cubu -f file.trl". The file "segmentation_lamp.trl" should work as example.