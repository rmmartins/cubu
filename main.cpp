#include "include/cpubundling.h"
#include "include/gdrawing.h"
#include "include/glutwrapper.h"
#include "include/gluiwrapper.h"

#include <math.h>
#include <iostream>
#include <string>
#include <time.h>


using namespace std;



int					fboSize;
GraphDrawing*		gdrawing_bund;
GraphDrawing*		gdrawing_orig;
GraphDrawing*		gdrawing_final;
float				scale = 1;
float				transX = 0, transY = 0;
CPUBundling*		bund; 
static int			show_points	= 0;
static int			show_edges	= 1;
static int			show_endpoints = 0;
static int			gpu_bundling = 1;	
static int			density_estimation = 1; 
static int			color_mode = 1;
static int			alpha_mode = 1;
static int			bundle_shape = 0;
static int			block_endpoints = 1;
static int			use_density_alpha = 0;
static float		relaxation = 0;
static GLUI*		glui;

enum 
{
		UI_SHOW_BUNDLES,
		UI_BUNDLE_ITERATIONS,
		UI_BUNDLE_MS_ITERATIONS,
		UI_BUNDLE_KERNEL,
		UI_BUNDLE_MS_KERNEL,
		UI_BUNDLE_EDGERES,
		UI_BUNDLE_SMOOTH,
		UI_BUNDLE_SMOOTH_ITER,
		UI_BUNDLE_SPEED,
		UI_BUNDLE_DENS_ESTIM,
		UI_BUNDLE_COLOR_MODE,
		UI_BUNDLE_ALPHA_MODE,
		UI_BUNDLE_CPU_GPU,
		UI_BUNDLE_SHAPE,
		UI_BUNDLE_SHOW_POINTS,
		UI_BUNDLE_SHOW_EDGES,
		UI_BUNDLE_SHOW_ENDPOINTS,
		UI_BUNDLE_LINEWIDTH,
		UI_BUNDLE_GLOBAL_ALPHA,
		UI_BUNDLE_BLOCK_ENDS,
		UI_BUNDLE_SMOOTH_ENDS,
		UI_BUNDLE_RELAXATION,
		UI_BUNDLE_DENSITY_ALPHA,
		UI_QUIT
};


void display_cb();
void buildGUI(int);
void bundle();
void postprocess();



int main(int argc,char **argv)
{
	char* graphfile = 0;
	fboSize   = 512;	
	

	
	for (int ar=1;ar<argc;++ar)
	{
		string opt = argv[ar];
		if (opt=="-f")
		{
			++ar;
			graphfile = argv[ar];
		}
		else if (opt=="-i")
		{
			++ar;
			fboSize = atoi(argv[ar]);
		}
	}
	

	gdrawing_bund  = new GraphDrawing();						//Make two graphs: the original one, and the bundled one
	gdrawing_orig  = new GraphDrawing();				
	gdrawing_final = new GraphDrawing();

	bool ok = gdrawing_orig->readTrails(graphfile);				//Read some input graph
	if (!ok) 
	{
		cout<<"Error: cannot open file "<<graphfile<<endl;
		exit(1);
	}
	
	bund = new CPUBundling(fboSize);							//Create bundling engine; we'll use it for several graph bundling tasks in this class
	bund->block_endpoints = block_endpoints;
	bund->density_estimation = (CPUBundling::DENSITY_ESTIM)density_estimation;

	gdrawing_orig->normalize(Point2d(fboSize,fboSize),0.1);		//Fit graph nicely in the graphics window
	gdrawing_orig->draw_points	   = show_points;
	gdrawing_orig->draw_edges      = show_edges;
	gdrawing_orig->draw_endpoints  = show_endpoints;
	gdrawing_orig->color_mode	   = (GraphDrawing::COLOR_MODE)color_mode;
	gdrawing_orig->alpha_mode	   = (GraphDrawing::ALPHA_MODE)alpha_mode;
	gdrawing_orig->densityMap      = bund->h_densityMap;
	gdrawing_orig->densityMapSize  = fboSize;
	gdrawing_orig->use_density_alpha = use_density_alpha;

	*gdrawing_final = *gdrawing_orig;									

    glutInitWindowSize(fboSize, fboSize); 
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_ALPHA); 
    glutInit(&argc, argv); 
	int mainWin = glutCreateWindow("Graph bundling"); 
    glutDisplayFunc(display_cb);

	buildGUI(mainWin);

	glutMainLoop(); 	

	delete gdrawing_bund;
	delete gdrawing_orig;
	delete gdrawing_final;
    return 0;
}





void display_cb() 
{
    glClearColor(1,1,1,1);											//Reset main GL state to defaults
    glClear(GL_COLOR_BUFFER_BIT); 
    glDisable(GL_LIGHTING);											
    glDisable(GL_DEPTH_TEST); 

    glViewport(0,0,fboSize,fboSize); 
    glMatrixMode(GL_PROJECTION);									//Setup projection matrix
    glLoadIdentity(); 
    gluOrtho2D(0,fboSize,0,fboSize); 
    glMatrixMode(GL_MODELVIEW);										//Setup modelview matrix
    glLoadIdentity(); 
    glScalef(scale,scale,1);									
    glTranslatef(transX,transY,0);
	
	gdrawing_final->draw();
	
    glutSwapBuffers();												// All done
}


void bundle()
{
	*gdrawing_bund = *gdrawing_orig;								//Copy the original drawing to gdrawing_bund, since we want to bundle it,
																	//but we don't want to alter the original drawing.
	bund->setInput(gdrawing_bund);									//Bundle the graph drawing

	if (gpu_bundling)												//Use the CPU or GPU method
		bund->bundleGPU();	
	else	
		bund->bundleCPU();
	
	postprocess();													//Postprocess the bundling before display
}

void postprocess()													//Postprocess the bundling before display
{
	*gdrawing_final = *gdrawing_bund;								//Don't modify the bundled graph, copy it
	gdrawing_final->interpolate(*gdrawing_orig,relaxation);			//Relax bundling towards original graph
}

void control_cb(int ctrl)
{
	switch(ctrl)
	{
	case UI_BUNDLE_ITERATIONS:
	case UI_BUNDLE_MS_ITERATIONS:
	case UI_BUNDLE_KERNEL:
	case UI_BUNDLE_MS_KERNEL:
	case UI_BUNDLE_EDGERES:
	case UI_BUNDLE_SMOOTH:
	case UI_BUNDLE_SMOOTH_ITER:
	case UI_BUNDLE_SMOOTH_ENDS:
	case UI_BUNDLE_SPEED:
	case UI_BUNDLE_CPU_GPU:
		bundle();
		break;
	case UI_BUNDLE_DENS_ESTIM:
		bund->density_estimation = (CPUBundling::DENSITY_ESTIM)density_estimation;
		bundle();
		break;	
	case UI_BUNDLE_SHAPE:
		bund->initEdgeProfile((CPUBundling::EDGE_PROFILE)bundle_shape);
		bundle();
		break;
	case UI_BUNDLE_BLOCK_ENDS:
		bund->block_endpoints = block_endpoints;
		bund->initEdgeProfile((CPUBundling::EDGE_PROFILE)bundle_shape);
		bundle();
		break;		

	case UI_BUNDLE_COLOR_MODE:
		gdrawing_final->color_mode = gdrawing_orig->color_mode = (GraphDrawing::COLOR_MODE)color_mode;
		break;	
	case UI_BUNDLE_ALPHA_MODE:
		gdrawing_final->alpha_mode = gdrawing_orig->alpha_mode = (GraphDrawing::ALPHA_MODE)alpha_mode;
		break;	
	case UI_BUNDLE_SHOW_POINTS:
		gdrawing_final->draw_points = gdrawing_orig->draw_points = show_points;
		break;	
	case UI_BUNDLE_SHOW_EDGES:
		gdrawing_final->draw_edges = gdrawing_orig->draw_edges = show_edges;
		break;
	case UI_BUNDLE_SHOW_ENDPOINTS:
		gdrawing_final->draw_endpoints = gdrawing_orig->draw_endpoints = show_endpoints;
		break;	
	case UI_BUNDLE_GLOBAL_ALPHA:
		gdrawing_final->global_alpha = gdrawing_orig->global_alpha;
		break;
	case UI_BUNDLE_DENSITY_ALPHA:
		gdrawing_final->use_density_alpha = gdrawing_orig->use_density_alpha = use_density_alpha;
		break;	

	case UI_BUNDLE_RELAXATION:
		postprocess();
		break;	
	case UI_QUIT:
		exit(0);
		break;	
	}
	
	glui->post_update_main_gfx();
}

void buildGUI(int mainWin)
{
	GLUI_Panel *pan,*pan2,*pan3;											//Construct GUI:
	GLUI_Scrollbar* scr;
	glui = GLUI_Master.create_glui("CUDA Bundling");		

	GLUI_Rollout* ui_bundling = glui->add_rollout("Bundling",false);		//4. Panel "Bundling":

	pan3 = glui->add_panel_to_panel(ui_bundling,"Main bundling");
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Iterations");
	glui->add_column_to_panel(pan,false);
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->niter,UI_BUNDLE_ITERATIONS,control_cb);
	scr->set_int_limits(0,40);
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Kernel size");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->h,UI_BUNDLE_KERNEL,control_cb);
	scr->set_float_limits(3,40);
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Smoothing factor");
	glui->add_column_to_panel(pan,false);
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->lambda,UI_BUNDLE_SMOOTH,control_cb);
	scr->set_float_limits(0,1);
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Smoothing iterations");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->liter,UI_BUNDLE_SMOOTH_ITER,control_cb);
	scr->set_int_limits(0,10);

	pan3 = glui->add_panel_to_panel(ui_bundling,"Ends bundling");
	new GLUI_Checkbox(pan3,"Block endpoints",&block_endpoints,UI_BUNDLE_BLOCK_ENDS,control_cb);			
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Iterations");
	glui->add_column_to_panel(pan,false);
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->niter_ms,UI_BUNDLE_MS_ITERATIONS,control_cb);
	scr->set_int_limits(0,40);
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Kernel size");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->h_ms,UI_BUNDLE_MS_KERNEL,control_cb);
	scr->set_float_limits(3,80);
	pan = glui->add_panel_to_panel(pan3,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Smoothing factor");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->lambda_ends,UI_BUNDLE_SMOOTH_ENDS,control_cb);
	scr->set_float_limits(0,1);	


	pan2 = glui->add_panel_to_panel(ui_bundling,"General options");

	pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Edge sampling");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->spl,UI_BUNDLE_EDGERES,control_cb);
	scr->set_float_limits(3,50);

	pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Advection speed");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&bund->eps,UI_BUNDLE_SPEED,control_cb);
	scr->set_float_limits(0,1);

	pan = glui->add_panel_to_panel(pan2,"Density estimation");
	GLUI_RadioGroup* ui_dens_estim = new GLUI_RadioGroup(pan,&density_estimation,UI_BUNDLE_DENS_ESTIM,control_cb);
	new GLUI_RadioButton(ui_dens_estim,"Exact");
	new GLUI_RadioButton(ui_dens_estim,"Fast");

	pan = glui->add_panel_to_panel(pan2,"Bundle shape");
	GLUI_RadioGroup* ui_bundle_shape = new GLUI_RadioGroup(pan,&bundle_shape,UI_BUNDLE_SHAPE,control_cb);
	new GLUI_RadioButton(ui_bundle_shape,"FDEB");
	new GLUI_RadioButton(ui_bundle_shape,"HEB");

	pan = glui->add_panel_to_panel(pan2,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Relaxation");
	glui->add_column_to_panel(pan,false);	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&relaxation,UI_BUNDLE_RELAXATION,control_cb);
	scr->set_float_limits(0,1);
	
		
	new GLUI_Checkbox(pan2,"GPU method", &gpu_bundling,UI_BUNDLE_CPU_GPU,control_cb);			


    new GLUI_Button(glui,"Quit",UI_QUIT,control_cb);						//5. "Quit" button:

	glui->add_column(true);													//--------------------------------------------------------
	GLUI_Rollout* ui_drawing = glui->add_rollout("Drawing",false);			//6. Roll-out "Drawing":
	pan = glui->add_panel_to_panel(ui_drawing,"Draw what");
	new GLUI_Checkbox(pan,"Edges", &show_edges,UI_BUNDLE_SHOW_EDGES,control_cb);			
	new GLUI_Checkbox(pan,"Control points", &show_points,UI_BUNDLE_SHOW_POINTS,control_cb);			
	new GLUI_Checkbox(pan,"End points", &show_endpoints,UI_BUNDLE_SHOW_ENDPOINTS,control_cb);			
	pan = glui->add_panel_to_panel(ui_drawing,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Line width");
	glui->add_column_to_panel(pan,false);
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&gdrawing_orig->line_width,UI_BUNDLE_LINEWIDTH,control_cb);
	scr->set_float_limits(1,5);
	pan = glui->add_panel_to_panel(ui_drawing,"",GLUI_PANEL_NONE);
	new GLUI_StaticText(pan,"Global alpha");
	glui->add_column_to_panel(pan,false);
	new GLUI_Checkbox(ui_drawing,"Density-modulated alpha", &use_density_alpha,UI_BUNDLE_DENSITY_ALPHA,control_cb);			
	
	
	scr = new GLUI_Scrollbar(pan,"",GLUI_SCROLL_HORIZONTAL,&gdrawing_orig->global_alpha,UI_BUNDLE_GLOBAL_ALPHA,control_cb);
	scr->set_float_limits(0,1);
	pan = glui->add_panel_to_panel(ui_drawing,"Coloring");
	GLUI_RadioGroup* ui_color = new GLUI_RadioGroup(pan,&color_mode,UI_BUNDLE_COLOR_MODE,control_cb);
	new GLUI_RadioButton(ui_color,"Grayscale");
	new GLUI_RadioButton(ui_color,"Rainbow (blue-red)");
	new GLUI_RadioButton(ui_color,"Rainbow (red-blue)");
	pan = glui->add_panel_to_panel(ui_drawing,"Transparency");
	GLUI_RadioGroup* ui_alpha = new GLUI_RadioGroup(pan,&alpha_mode,UI_BUNDLE_ALPHA_MODE,control_cb);
	new GLUI_RadioButton(ui_alpha,"Constant");
	new GLUI_RadioButton(ui_alpha,"Mark short edges");
	new GLUI_RadioButton(ui_alpha,"Mark long edges");


	glui->set_main_gfx_window(mainWin);										//Link GLUI with GLUT (seems needed)	
}
