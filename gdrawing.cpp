#ifdef PLATFORM_WIN
#define NOMINMAX
#define _USE_MATH_DEFINES
#include <time.h>
#include <algorithm>
#endif

#include "include/gdrawing.h"
#include "include/graph.h"
#include "include/glwrapper.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


using namespace std;



static float dir2angle(const Point2d& d)
{
	Point2d x = d; x.normalize();

	float cosa = x.x;
	float sina = x.y;
	
	float a;
	if (sina>0)
	{
		a = acos(cosa);
	}
	else
	{
		a = M_PI + acos(cosa);
	}
	
	return a;
}



float GraphDrawing::Polyline::length() const
{
	int NP  = size();
	float l = 0;
	for(int i=1;i<NP;++i)
	   l += (*this)[i].dist((*this)[i-1]);
	return l;
}   

void GraphDrawing::Polyline::interpolate(const Polyline& target,float lambda)
{
    int   N  = size();
	int   Nt = target.size();
	float L  = length();
	
	Polyline res = *this;

	float l = 0;	
	const Point2d* prev = &(*this)[0];
	for(int i=1;i<N-1;++i)						//Displace this' points (except endpoints):
	{
	   const Point2d* crt = &(*this)[i];	
	   l += crt->dist(*prev);	   
	   float t = l/L;							//t = arc-length param of i-th point of this (in [0,1])

	   int    j = t*(Nt-1);						//Find corresponding points of target between which 'crt' lies
	   float t1 = float(j)/(Nt-1);				//WARNING: This is not 100% correct, it assumes that 'target' is uniformly sampled
	   float t2 = float(j+1)/(Nt-1);
		   
	   float   u = (t-t1)/(t2-t1);				//Find target point 'p' corresponding to i-th point in this
	   Point2d p = target[j]*(1-u) + target[j+1]*u;	
	   
	   res[i] = (*crt)*(1-lambda) + p*lambda;		
												//Interpolate between 'p' and i-th point of this, using lambda
	   prev = crt;											
	}
	
	*this = res;								//Replace this by interpolated curve
}



void GraphDrawing::float2alpha(float value,float& alpha) const
{
		switch(alpha_mode)
		{
		case ALPHA_CONSTANT:
		alpha = 1; 
		break;
		
		case ALPHA_VALUE:
		alpha = value;
		break;

		case ALPHA_INVERSE_VALUE:
		alpha = 1-value;
		break;
		}
}


void GraphDrawing::float2rgb(float value,float& R,float& G,float& B) const	
{	
   switch(color_mode)
   {
		case GRAYSCALE:
		R = G = B = value;
		break;
		
		case RAINBOW:
		{
		const float dx=0.8f;
		value = (6-2*dx)*value+dx;
		R = max(0.0f,(3-(float)fabs(value-4)-(float)fabs(value-5))/2);
		G = max(0.0f,(4-(float)fabs(value-2)-(float)fabs(value-4))/2);
		B = max(0.0f,(3-(float)fabs(value-1)-(float)fabs(value-2))/2);
		break;
		}

		case INVERSE_RAINBOW:
		{
		const float dx=0.8f;
		value = (6-2*dx)*(1-value)+dx;
		R = max(0.0f,(3-(float)fabs(value-4)-(float)fabs(value-5))/2);
		G = max(0.0f,(4-(float)fabs(value-2)-(float)fabs(value-4))/2);
		B = max(0.0f,(3-(float)fabs(value-1)-(float)fabs(value-2))/2);
		break;
		}
   }	 
}



void GraphDrawing::interpolate(const GraphDrawing& target,float t)
{
	for(int i=0,NP=drawing.size();i<NP;++i)									//Interpolate all edges in this w.r.t. their counterparts in 'target':
	{
		Row&       row  = drawing[i];
		const Row& grow = target.drawing[i];

		Row::const_iterator git=grow.begin();
		for(Row::const_iterator it=row.begin();it!=row.end();++it,++git)
		{
			Polyline&        pl = *it->second;
			const Polyline& gpl = *git->second;
			pl.interpolate(gpl,t);											//Interpolate edge 'pl' towards 'gpl'. Result goes into 'gpl'
		}
	}	
}





GraphDrawing::GraphDrawing(): val_min(0),val_max(1),draw_points(false),draw_edges(true),draw_endpoints(false),num_edges(0),line_width(1),global_alpha(1),
							  color_mode(RAINBOW),alpha_mode(ALPHA_VALUE),densityMap(0),densityMapSize(0),use_density_alpha(false)	
{
	srand(time(0));
}


GraphDrawing::~GraphDrawing()
{
	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		delete it->second;
	}
}	


const GraphDrawing& GraphDrawing::operator=(const GraphDrawing& rhs)
{
	if (&rhs==this) return *this;
	
	int NR = rhs.numNodes();

	drawing.resize(NR);												//Allocate #nodes
	draw_order.clear();

	for(int i=0;i<NR;++i)											//Add edges for each node: To make sure we don't add an edge twice,
	{																//we only add edges from a node i to all nodes with j>i
		const Row& rhs_row = rhs(i);
		Row&       row     = (*this)(i);
		row.clear();
		
		for(Row::const_iterator it=rhs_row.begin();it!=rhs_row.end();++it)
		{
		   int            key = it->first;	
		   const Polyline* pl = it->second;
		   
		   Polyline* line = new Polyline(*pl);						//Copy ctor

		   Row::const_iterator ins = row.insert(make_pair(key,line)).first;

		   draw_order.insert(make_pair(line->value,ins->second));			//Add edges sorted on increasing 'value' (for later drawing etc)
		}
	}	

	val_min			= rhs.val_min;
	val_max			= rhs.val_max;
	draw_points		= rhs.draw_points;
	draw_edges		= rhs.draw_edges;
	draw_endpoints  = rhs.draw_endpoints;
	num_edges		= rhs.num_edges;
	line_width		= rhs.line_width;
	global_alpha	= rhs.global_alpha;
	color_mode		= rhs.color_mode;
	alpha_mode		= rhs.alpha_mode;
	densityMap		= rhs.densityMap;
	densityMapSize	= rhs.densityMapSize;
	use_density_alpha	= rhs.use_density_alpha;
	

	return *this;
}

void GraphDrawing::normalize(const Point2d& dim,float border)
{
	Point2d min_p = Point2d(1.0e+7,1.0e+7);
	Point2d max_p = Point2d(-1.0e+7,-1.0e+7);

	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		const Polyline& pl = *it->second;
		for(int i=0,N=pl.size();i<N;++i)
		{
			const Point2d& p = pl[i];
			min_p = Point2d(std::min(min_p.x,p.x),std::min(min_p.y,p.y));
			max_p = Point2d(std::max(max_p.x,p.x),std::max(max_p.y,p.y));
		}	
	}
	
	Point2d rng = max_p-min_p;
	float scale;
	if (rng.x>rng.y)
	{
		scale  = (1-border)*dim.x/rng.x;
	}
	else
	{
		scale  = (1-border)*dim.y/rng.y;
	}

	Point2d t;		
	t.x    = (dim.x-scale*rng.x)/2;
	t.y    = (dim.y-scale*rng.y)/2;

	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		Polyline& pl = *((Polyline*)it->second);
		for(int i=0,N=pl.size();i<N;++i)
		{
			pl[i] = (pl[i]-min_p)*scale + t;
		}
	}	  
}	



void GraphDrawing::resample(float delta,float jitter)				//Resample drawing for inter-point distance 'delta' +/- 'jitter'
{
	for(int i=0,NP=numNodes();i<NP;++i)
	{
		Row& row = (*this)(i);

		for(Row::iterator it=row.begin(),ie=row.end();it!=ie;++it)
		{
			Polyline& pl = *it->second;
			resample(pl,delta,jitter);
		}
	}	
}

void GraphDrawing::build(const Graph* m,const PointSet* points)		//Build graph drawing from Graph and point positions
{
	int NR = m->numRows();

	drawing.resize(NR);												//Allocate #nodes
	draw_order.clear();

    num_edges = 0;
	val_min = 1.0e+6;
	val_max = -1.0e+6;
	for(int i=0;i<NR;++i)											//Add edges for each node: To make sure we don't add an edge twice,
	{																//we only add edges from a node i to all nodes with j>i
		const Graph::Row& mrow = (*m)(i);
		Row& drow = (*this)(i);
		
		for(Graph::Row::const_iterator it=mrow.begin();it!=mrow.end();++it)
		{
		   int     j = it->first;
		   if (j<=i) continue;
		   float val = it->second;

		   Polyline* line = new Polyline(val);
		   line->push_back((*points)[i]);
		   line->push_back((*points)[j]);
		   Row::const_iterator ins = drow.insert(make_pair(j,line)).first;
		   draw_order.insert(make_pair(val,ins->second));			//Add edges sorted on increasing 'value' (for later drawing etc)
		   val_min = std::min(val_min,val);
		   val_max = std::max(val_max,val);
		   ++num_edges;
		}
	}
}




void GraphDrawing::resample(Polyline& pl,float delta,float jitter)					
{
	int      NP   = pl.size();
	float    crtd = delta;
	Point2d  prev = pl[0];				//'prev' is always on segment (i-1,i)

	Polyline nl(pl.value,pl.size());	//the new, resampled, polyline (allocate some initial space so resizing is fast)
	nl.push_back(prev);					//add 1st point of input polyline to resampled one
	
	for(int i=1;i<NP;)					//resample input polyline:
	{
		const Point2d& crt = pl[i];
		float newdist = crt.dist(prev);	//distance from last resampled point to i-th input point
		
		if (newdist<crtd)				//i-th input point closer to 'prev' than remaining fraction of delta:
		{								//skip i-th point
			crtd -= newdist;
			prev  = crt;
			++i;
		}
		else							//i-th input point farther from 'prev' than remaining fraction of delta:
		{
			float t = crtd/newdist;	
			prev = prev*(1-t) + crt*t;	//add new point to resampling
			nl.push_back(prev);
			
			float r = (float(rand())/RAND_MAX)*2-1;
										//generate random number in [-1..1]	
			crtd = delta*(1+r*jitter);	
										//reset delta to whatever we want to achieve
										//use here a noise equal to jitter*delta
		}
	}	
	
	if (crtd<delta)
	  nl.push_back(pl[NP-1]);
	
	pl = nl;							//return resampled line to caller
}


void GraphDrawing::draw() const			//Draw this
{	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	int N_max = 0;
	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		const Polyline& pl = *(it->second);
		int N = pl.size();
		N_max = std::max(N,N_max);
	}

	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		const Polyline& pl = *(it->second);
		int N = pl.size();
		float val = 1-(pl.value-val_min)/(val_max-val_min);			//val: 1 for shortest edges, 0 for longest

		float r,g,b,alpha;
		float2rgb(val,r,g,b);										//Get edge base-color	
		float2alpha(val,alpha);										//Get edge base-alpha
		alpha *= global_alpha;										//Modulate alpha with global transparency
					
		if (draw_edges)												//Draw edges:
		{		
			glLineWidth(line_width);
			glEnable(GL_LINE_SMOOTH);
			glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
			glShadeModel(GL_SMOOTH);
			glBegin(GL_LINE_STRIP);

			for(int j=0;j<N;++j)
			{
			   float t = float(j)/(N-1);							//t: 0..1 over edge (length parameterization)
			   t = pow(1-2*fabs(t-0.5),0.5);						//t: 0..1..0 over edge (cushion profile)
			   t = (1-val)*1 + val*t;								//t: const (1) for longest edges, cushion for shortest edges
			   float ta = val*1 + (1-val)*t;						//ta: const (1) for shortest edges, cushion for longest edges
			   
			   float xx = (use_density_alpha)? densityMap[int(pl[j].y)*densityMapSize+int(pl[j].x)] : 1;
			   
			   glColor4f(r*t,g*t,b*t,alpha*ta*xx);						//3. hue,alpha = f(length)
			   glVertex2f(pl[j].x,pl[j].y);
			}
			glEnd();
			glDisable(GL_LINE_SMOOTH);
			glLineWidth(1.0f);
		}

		if (draw_points)											//Draw control points:
		{
			glEnable(GL_POINT_SMOOTH);
			glPointSize(1.5);
			glColor3f(1,0,0);
			glBegin(GL_POINTS);
			for(int j=0;j<N;++j)
			   glVertex2f(pl[j].x,pl[j].y);
			glEnd();
			glPointSize(1);
			glDisable(GL_POINT_SMOOTH);
		}
	}	
	

	if (draw_endpoints)
	for(DrawingOrder::const_iterator it=draw_order.begin();it!=draw_order.end();++it)
	{
		const Polyline& pl = *(it->second);
		int N = pl.size();

		glEnable(GL_POINT_SMOOTH);
		glPointSize(1.5);
		glColor3f(0,1,0);
		glBegin(GL_POINTS);
		glVertex2f(pl[0].x,pl[0].y);
		glVertex2f(pl[N-1].x,pl[N-1].y);
		glEnd();
		glPointSize(1);
		glDisable(GL_POINT_SMOOTH);
	}
	
	glDisable(GL_BLEND);
}


void GraphDrawing::saveTrails(const char* fn) const
{
	FILE* fp = fopen(fn,"w");
	if (!fp) return;

	int lcount = 0;
	for(int i=0,NP=drawing.size();i<NP;++i)
	{
		const Row& row = drawing[i];
		for(Row::const_iterator it=row.begin();it!=row.end();++it)
		{
			fprintf(fp,"%d: ",lcount++);
			const Polyline& pl = *it->second;
			for(int j=0;j<pl.size();++j)
			{
			   const Point2d& p = pl[j];	
			   fprintf(fp,"%f %f ",p.x,p.y);
			}  
			fprintf(fp,"\n");
		}
	}	
	
	fclose(fp);
}


bool GraphDrawing::readTrails(const char* fn)
{
	FILE* fp = fopen(fn,"r");
	if (!fp) return false;
	
	draw_order.clear();
	
	Polyline* line = 0;
	int NP=0;
	
	for(int lidx=-1;;)
	{
	   char val[128];
	   int c = fscanf(fp,"%s",val);
	   
	   bool eof = (c!=1);	
	   bool eol = (eof) || (val[strlen(val)-1]==':');

	   if (eol)														//beginning of new polyline edge:
	   {
		   if (line)												//Finish currently built polyline
		   {
			   drawing.push_back(GraphDrawing::Row());
			   GraphDrawing::Row& row = drawing[drawing.size()-1];
			   			   
			   int nidx = 2*lidx+1;
			   GraphDrawing::Row::const_iterator ins = row.insert(make_pair(nidx,line)).first;
			   
			   float imp   = line->length();
			   line->value = imp;									//Now that the edge is ready, give it an importance
			   			   			   
			   draw_order.insert(make_pair(line->value,ins->second));	//Add edges sorted on increasing 'value' (for later drawing etc)
			   val_min = std::min(val_min,line->value);
		       val_max = std::max(val_max,line->value);
		   }
		   
		   if (eof) break;
		   ++lidx;	


		   float value = lidx;
		   
		   line = new Polyline(value);								//add new line, with user-defined importance value
	   }	
	   else															//adding new point to current polyline:
	   {
		   if (val[0]!='(' && val[0]!='<') 
		   {
		     Point2d p;
		     p.x = atof(val);
		     fscanf(fp,"%s",val);
		     p.y = atof(val);
		     line->push_back(p);
			 NP++;
		   }	 
	   }
	}
	
	fclose(fp);
	
	cout<<"Total control points in input: "<<NP<<endl;
	
	return true;
}
	



