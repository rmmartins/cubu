#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/cudawrapper.h"
#include <curand_kernel.h>
#include "include/gpubundling.h"
#include "cuPrintf.cu"


texture<float,cudaTextureType2D,cudaReadModeElementType>		 texDensity;						//State var:  the float 2D density texture
texture<unsigned int,cudaTextureType1D,cudaReadModeElementType>	 texCount;							//!!
texture<BundlingPoint,cudaTextureType1D,cudaReadModeElementType> texSites;							//State var:  the float2 1D sites vector
texture<int,cudaTextureType1D,cudaReadModeElementType>			 texStarts;							//State var:  
texture<int,cudaTextureType1D,cudaReadModeElementType>			 texEdges;							//State var:  start-offsets of all edges in texSites[]
texture<float,cudaTextureType1D,cudaReadModeElementType>		 texEdgeProfile;					//State var:  the edge profile (controlling the advection along an edge)

__constant__ float												 c_Kernel[MAX_KERNEL_LENGTH];		//State var:  the kernel data (stored as constant for speed)
__constant__ int kernel_radius;																		//State var:  the current kernel radius
__constant__ int imageW,imageH;																		//State vars: sizes of the image used allover through the code
__device__   int numControlPoints;



//--- GPU-specific defines -----------------------------------------------------------------

#define IMAD(a, b, c) ( __mul24((a), (b)) + (c) )			//Maps to a single instruction on G8x / G9x / G10x

inline int iDivUp(int a, int b)								//Round a / b to nearest higher integer value
{
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

inline int iAlignUp(int a, int b)							//Align a to nearest higher multiple of b
{
    return (a % b != 0) ?  (a - a % b + b) : a;
}



//----  Row convolution filter  -------------------------------------------------
__global__ void convolutionRowsKernel(float* output)
{
    const   int ix = IMAD(blockDim.x, blockIdx.x, threadIdx.x);
    const   int iy = IMAD(blockDim.y, blockIdx.y, threadIdx.y);
    if (ix >= imageW || iy >= imageH) return;							//Careful not to index outside image

    const float  x = (float)ix + 0.5f;
    const float  y = (float)iy + 0.5f;
    float sum = 0;

	for(short k = -kernel_radius; k <= kernel_radius; ++k)
	sum += tex2D(texDensity,x+k,y)*c_Kernel[kernel_radius-k];
    
    output[IMAD(iy,imageW,ix)] = sum;	
}

void convolutionRows(float* output,cudaArray *a_Src,int imageW,int imageH)
{
    dim3 threads(NTHREADS_X,NTHREADS_Y);
    dim3 blocks(iDivUp(imageW,threads.x),iDivUp(imageH,threads.y));

    cudaBindTextureToArray(texDensity,a_Src);
    convolutionRowsKernel<<<blocks,threads>>>(output);
	cudaUnbindTexture(texDensity);
	cudaThreadSynchronize();
}


//----   Column convolution filter  -------------------------------------------------
__global__ void convolutionColumnsKernel(float* output)
{
    const   int ix = IMAD(blockDim.x, blockIdx.x, threadIdx.x);
    const   int iy = IMAD(blockDim.y, blockIdx.y, threadIdx.y);
    if (ix >= imageW || iy >= imageH) return;							//Careful not to index outside image

    const float  x = (float)ix + 0.5f;
    const float  y = (float)iy + 0.5f;
    float sum = 0;

	for(short k = -kernel_radius; k <= kernel_radius; k++)
		sum += tex2D(texDensity,x,y+k)*c_Kernel[kernel_radius-k];

	output[IMAD(iy,imageW,ix)] = sum;
}

void convolutionColumns(float* output,cudaArray *a_Src,int imageW,int imageH)
{
    dim3 threads(NTHREADS_X,NTHREADS_Y);	
    dim3 blocks(iDivUp(imageW,threads.x), iDivUp(imageH,threads.y));

    cudaBindTextureToArray(texDensity,a_Src);
    convolutionColumnsKernel<<<blocks,threads>>>(output);
	cudaUnbindTexture(texDensity);
	cudaThreadSynchronize();
}




__global__ void kernelSiteInitCount(unsigned int* output,int numpts)		//Init the image output[] with the #sites at ech pixel
{
    int offs = IMAD(blockIdx.x, blockDim.x, threadIdx.x);

	if (offs < numpts)														//careful not to index outside the site-vector..
	{	
	  const BundlingPoint p = tex1Dfetch(texSites,offs);					//find coords of current site	  
	  if (p.x<0) return;													//this is a marker: nothing to do
	  
	  int site_offs = IMAD(imageW,int(p.y),int(p.x));						//Increment site-count for pixel under it
	  atomicAdd(output+site_offs,1);										//REMARK: This seems expensive, but it only occurs when sites DO overlap...	
	}
}

__global__ void kernelSiteInitFloat(float* output,int numpts,float value)	//Init the image output[] with 'height' at all site locations
{
	int offs = IMAD(blockIdx.x,blockDim.x,threadIdx.x);
	
	if (offs < numpts)
	{	
	  const BundlingPoint p  = tex1Dfetch(texSites,offs);					//find coords of current site	  
	  if (p.x<0) return;													//this is a marker: nothing to do
	  
	  int site_offs = IMAD(imageW,int(p.y),int(p.x));
	  output[site_offs] += value;											//Set pixel under site to 'height'
	}																		//WARNING: This underestimates density where multiple sites fall on the same pixel
																			//due to the fact that += is not atomic on threads that want to write to the same pixel
}




__global__ void kernelSiteInitCount2Float(float* output,int imageW)			//Copy count image (from texCount) to floating-point image output[]
{
    const   int ix = IMAD(blockDim.x, blockIdx.x, threadIdx.x);
    const   int iy = IMAD(blockDim.y, blockIdx.y, threadIdx.y);
    if (ix >= imageW || iy >= imageH) return;								//Careful not to index outside image

	const int offs = IMAD(iy,imageW,ix); 
    output[offs] = tex1Dfetch(texCount,offs); 
}

extern "C" void initializeSiteLocations(CPUBundling::DENSITY_ESTIM dens_estim,cudaArray* a_Src,float* d_Output,unsigned int* d_Count,BundlingPoint* d_points,int npoints,float value,int imageW,int imageH)
{
    cudaMemcpyToSymbol(::imageW, &imageW, sizeof(int));					//Store these state-vars in const memory, for fast access later
    cudaMemcpyToSymbol(::imageH, &imageH, sizeof(int));
	
	int threads  = NTHREADS;											//Prepare the site-init kernel: this reads a vector of 2D sites
	int numpts_b = iAlignUp(npoints,threads);							//Find higher multiple of blocksize than # sites
	int blocks   = numpts_b/threads;
		
	cudaBindTexture(0,texSites,d_points);								//Bind 2D sites to a 1D texture	
	
	if (dens_estim==CPUBundling::DENSITY_FAST)							//Fast density estimation: simply record if there's no or one..more sites/pixel
	{
		cudaMemset(d_Output,0,sizeof(float)*imageW*imageH);				 //Zero the density texture
		kernelSiteInitFloat<<<blocks,threads>>>(d_Output,npoints,value); //Set all site-locations in the density map to 'value'
	}
	else	//DENSITY_EXACT												//Exact density estimation, pass 1
	{																	//Count #sites/pixel (slower, uses atomic ops)
		cudaMemset(d_Count,0,sizeof(unsigned int)*imageW*imageH);					
		kernelSiteInitCount<<<blocks,threads>>>(d_Count,npoints);		//Count #sites/pixel in d_Count
	}

	cudaThreadSynchronize();
	cudaUnbindTexture(texSites);										//Done with the sites

	if (dens_estim==CPUBundling::DENSITY_EXACT)							//Exact density estimation, pass 2
	{
		dim3 threads(NTHREADS_X,NTHREADS_Y);
		dim3 blocks(iDivUp(imageW,threads.x),iDivUp(imageH,threads.y));	
		cudaBindTexture(0,texCount,d_Count);
		kernelSiteInitCount2Float<<<blocks,threads>>>(d_Output,imageW);	//Simply copy d_Count to the floating-point d_Output
		cudaThreadSynchronize();
		cudaUnbindTexture(texCount);
	}
	
	cudaMemcpyToArray(a_Src,0,0,d_Output,imageW*imageH*sizeof(float),cudaMemcpyDeviceToDevice);	
}




__global__ void kernelAdvectSites(BundlingPoint* output,float h,float numpts)
{
    int offs = IMAD(blockIdx.x, blockDim.x, threadIdx.x);
	
	if (offs<numpts)
	{
		BundlingPoint p   = tex1Dfetch(texSites,offs);						//Get current point to advect
		
		if (p.x<0)															//Marker: copy it, don't advect it
		{
		    output[offs] = p;
		}
		else																//Regular point: advect it
		{
			float  v_d = tex2D(texDensity,p.x,p.y-1);						//Get density at that point and at its nbs
			float  v_l = tex2D(texDensity,p.x-1,p.y);
			float  v_r = tex2D(texDensity,p.x+1,p.y);
			float  v_t = tex2D(texDensity,p.x,p.y+1);
		
			BundlingPoint g = make_float4(v_r-v_l,v_t-v_d,p.z,0);			//Compute density gradient, simple forward difference method

			const float eps = 1.0e-5;										//Ensures we don't next get 1/0 for 0-length vectors	
			float gn = rsqrtf(g.x*g.x+g.y*g.y+eps);							//Robustly normalize the gradient

			float dot=1;
			/*
			if (offs+1<numpts)
			{	
				BundlingPoint np   = tex1Dfetch(texSites,offs+1);				//Get current point to advect
				if (np.x>=0)
				{
					float2 tv = make_float2(np.x-p.x,np.y-p.y);
					float tvn =rsqrtf(tv.x*tv.x+tv.y*tv.y+eps);
					tv.x *= tvn;
					tv.y *= tvn;			
					dot = 1-pow(fabs(tv.x*g.x*gn + tv.y*g.y*gn),0.3f);
				}
			}
			*/

			float  k = dot*h*p.z*gn;
			g.x *= k; g.x += p.x;											//Advect current point
			g.y *= k; g.y += p.y;
					
			output[offs] = g;												//Write displaced point to 'output'
		}
	}
}




extern "C" void advectSites(BundlingPoint* out_points,BundlingPoint* in_points,int npoints,cudaArray* a_Src,int imageW,int imageH,float h)						
{																		//Advect the sites, one step, along its density gradient
	dim3 block = dim3(NTHREADS);										//Prepare the site-init kernel: this reads a vector of 2D sites
	int numpts_b = iAlignUp(npoints,block.x);							//Find higher multiple of blocksize than # sites
	dim3 grid  = dim3(numpts_b/block.x);								//Number of blocks, each of 'block' threads. Each thread advects a point.

	cudaBindTexture(0,texSites,in_points);								//Bind 2D sites to a 1D texture (for reading)
	cudaBindTextureToArray(texDensity,a_Src);							//Bind density to a 2D texture (for reading)

	kernelAdvectSites<<<grid,block>>>(out_points,h,npoints);			//Advect the sites from in_points to out_points	in the density gradient 
	cudaThreadSynchronize();
	
	cudaUnbindTexture(texDensity);
	cudaUnbindTexture(texSites);										//Done with the textures
}



__global__ void kernelSmoothLines(BundlingPoint* out_points,int npoints,float t,int L)
{
    int offs = IMAD(blockIdx.x, blockDim.x, threadIdx.x);

	if (offs>=npoints) return;												//Care not to index outside the site array

	out_points += offs;

	BundlingPoint crtp = tex1Dfetch(texSites,offs);							//Is current point an end-of-line marker? 
	if (crtp.x<0)															//If so, copy it w/o smoothing
	{
	   *out_points = crtp;
	}
	else																	//Smooth current point w.r.t. a window of [-L,L] points centered at it:
	{
	   float2 pc = make_float2(0,0);
	   unsigned char pcount = 0;

	   int km = (offs>L)? offs-L : 0;										//Make sure we don't pass left to 0-th point..
	   #pragma unroll	
	   for(int k=offs-1;k>=km;--k)											//Gather points 'upstream' until end-of-kernel or beginning-of-line:
	   {
		  BundlingPoint pinp = tex1Dfetch(texSites,k);
		  if (pinp.x<0) break;												//Stop at line-beginning
		  
		  pc.x += pinp.x;
		  pc.y += pinp.y;
		  ++pcount;
	   }
	   	
	   #pragma unroll	
	   for(int kM=offs+L;offs<=kM;++offs)									//Gather points 'downstream' until end-of-kernel or end-of-line:
	   {
		  BundlingPoint pinp = tex1Dfetch(texSites,offs);
		  if (pinp.x<0) break;												//Stop at line-end
		  
		  pc.x += pinp.x;
		  pc.y += pinp.y;
		  ++pcount;
	   }
	   
	   t *= crtp.z;

	   const float k = t/pcount;											//Linear interpolation between point and average of its neighbors
	   crtp.x *= 1-t;
	   crtp.x += pc.x*k;
	   crtp.y *= 1-t;
	   crtp.y += pc.y*k;
	   *out_points = crtp;
	}
}


extern "C" void smoothLines(BundlingPoint* out_points,BundlingPoint* in_points,int npoints,float t,float h,float filter_kernel,int niter)	//Laplacian smoothing of graph-drawing edges
{
	dim3 threads = dim3(NTHREADS);										//Prepare the smooth kernel: this reads a vector of 2D sites
	int numpts_b = iAlignUp(npoints,threads.x);							//Find npoints upper-rounded to a multiple of block.x
	dim3 blocks  = dim3(numpts_b/threads.x);							//Find #blocks fitting numpts_b


	const int L = int(filter_kernel/h);									//Compute 1D Laplacian filter size, in #points, which corresponds to 'filter_kernel' space units
	if (L==0) return;													//Don't do smoothing if filter-size is zero..

	BundlingPoint *out = out_points, *inp = in_points;
	for(int i=0;i<niter;++i)											//Perform several Laplacian iterations:	
	{
		cudaBindTexture(0,texSites,inp);								//Bind 2D sites to a 1D texture (for reading), unbinds any possibly-bound texture
		kernelSmoothLines<<<blocks,threads>>>(out,npoints,t,L);	
		cudaThreadSynchronize();			
		
		BundlingPoint* tmp = out;										//Swap input vs output for next iteration
		out = inp;
		inp = tmp;
	}
	cudaUnbindTexture(texSites);										//Done with the texture
	
	cudaUnbindTexture(texDensity);
}





extern "C" void convolutionGPU(float* d_Output,cudaArray* a_Src,int imageW,int imageH)
{
	//1. Convolve on rows (a_Src -> d_Output)
	cudaThreadSynchronize();

	convolutionRows(d_Output,a_Src,imageW,imageH);
	
    //2. Copy row-convolution to texture (d_Output -> a_Src)
    //   While CUDA kernels can't write to textures directly, this copy is inevitable	
	cudaMemcpyToArray(a_Src,0,0,d_Output,imageW*imageH*sizeof(float),cudaMemcpyDeviceToDevice);
	cudaThreadSynchronize();

    //3. Convolve on columns (a_Src -> d_Output)
	convolutionColumns(d_Output,a_Src,imageW,imageH);
	
	//4. Copy convolution result to a_Src; needed since a_Src is next read by advectSites()
	cudaMemcpyToArray(a_Src,0,0,d_Output,imageW*imageH*sizeof(float),cudaMemcpyDeviceToDevice);		
	cudaThreadSynchronize();
}



extern "C" void setConvolutionKernel(float* h_Kernel,int sz,int rad)
{
    cudaMemcpyToSymbol(c_Kernel, h_Kernel, sz * sizeof(float));
    cudaMemcpyToSymbol(kernel_radius, &rad, sizeof(int));
}



//----


__device__ inline float dist(const BundlingPoint& p,const BundlingPoint& q)
{
	return sqrtf((p.x-q.x)*(p.x-q.x)+(p.y-q.y)*(p.y-q.y));
}



__global__ void kernelResampleCount(int* e_count,int n_edges,float delta)
{
    int e_idx = IMAD(blockIdx.x, blockDim.x, threadIdx.x);		//Get edge number
	if (e_idx>=n_edges) return;									//Careful not to index outside edge-vector
	
	int				i    = tex1Dfetch(texStarts,e_idx);			//Find 1st edge-point in texSites
	BundlingPoint   prev = tex1Dfetch(texSites,i);				//First point on current edge		
	float		    crtd = delta;
	int			    n_newp = 1;									//Add 1st point of input polyline to resampled one
	++i;

	BundlingPoint crt = tex1Dfetch(texSites,i);			
	for(;;)													//resample input polyline:
	{
		float newdist = dist(crt,prev);				//distance from last resampled point to i-th input point
		
		if (newdist<crtd)							//i-th input point closer to 'prev' than remaining fraction of delta:
		{											//skip i-th point
			crtd -= newdist;
			prev  = crt;
			++i;
			crt = tex1Dfetch(texSites,i);		
			if (crt.x<0) break;
		}
		else										//i-th input point farther from 'prev' than remaining fraction of delta:
		{
			float t = crtd/newdist;	
			prev.x = prev.x*(1-t) + crt.x*t;		//add new point to resampling
			prev.y = prev.y*(1-t) + crt.y*t;		//add new point to resampling
			++n_newp;
			crtd = delta;							//reset delta to whatever we want to achieve
		}
	}	
	
	if (crtd<delta) ++n_newp;
	++n_newp;										//marker	
	
	e_count[e_idx] = n_newp;						//save #points/edge
}


__global__ void kernelResample(BundlingPoint* new_pts,int n_edges,float delta,float jitter,curandState* state)
{
    int e_idx = IMAD(blockIdx.x, blockDim.x, threadIdx.x);		//Get edge number
	if (e_idx>=n_edges) return;									//Careful not to index outside edge-vector
	
	int				i    = tex1Dfetch(texStarts,e_idx);			//Find 1st edge-point in texSites
	BundlingPoint   prev = tex1Dfetch(texSites,i);				//First point on current edge		
	float		    crtd = delta;
	int			    n_newp = tex1Dfetch(texEdges,e_idx);
	int				inxt = tex1Dfetch(texEdges,e_idx+1);
	int				NP   = inxt-n_newp-2;	
	if (NP<1)		NP=1;
	const int		EPSZ = EDGE_PROFILE_SIZE-1;
	
	new_pts += n_newp;											//Here will the resampled edge's points be placed
	
	*new_pts++ = prev;											//add 1st point of input polyline to resampled one
	++i;

	curandState lState = state[threadIdx.x];				//cache random generator for speed, since we'll modify it locally

	BundlingPoint crt = tex1Dfetch(texSites,i);		
	for(int j=1;;)											//resample input polyline:
	{
		float newdist = dist(crt,prev);						//distance from last resampled point to i-th input point
		
		if (newdist<crtd)									//i-th input point closer to 'prev' than remaining fraction of delta:
		{													//skip i-th point
			crtd -= newdist;
			prev  = crt;
			++i;
			crt = tex1Dfetch(texSites,i);		
			if (crt.x<0) break;
		}
		else												//i-th input point farther from 'prev' than remaining fraction of delta:
		{
			float r  = curand_uniform(&lState)*2-1;			//generate random number in [-1..1]	
			float t  = crtd/newdist;	
			float rt = t*(1+r*jitter);						//jitter currently-generated point
			BundlingPoint rp;	
			rp.x   = prev.x*(1-rt) + crt.x*rt;
			rp.y   = prev.y*(1-rt) + crt.y*rt;

			int pidx = int(j*EPSZ/NP);
			rp.z = tex1Dfetch(texEdgeProfile,pidx);			//apply edge profile on newly, resampled, edge
			*new_pts++ = rp;								//add new resampled point to output
			
			prev.x = prev.x*(1-t) + crt.x*t;				//keep NON-jittered point as next point;
			prev.y = prev.y*(1-t) + crt.y*t;				//this ensures we get here EXACTLY the same polyline sampling as in kernelResampleCount()
			
			crtd = delta;									//reset delta to whatever we want to achieve
			++j;
		}
	}	
	
	if (crtd<delta)
	  *new_pts++ = tex1Dfetch(texSites,i-1);
	  
	new_pts->x = -1;										//add end-of-line marker
	
	state[threadIdx.x] = lState;							//update random generator
}




__global__ void kernelOffs(int* edges,int n_edges)
{
	//if (blockIdx.x==0 && threadIdx.x==0)
	{
		int cprev = edges[0];											//2. From 1, compute start-offset of resampled edges. Knowing this allows us to parallelize
		edges[0] = 0;													//   the resampling and writing the resampled points (in pass 3 below)
		for(int i=1;i<n_edges;++i)										
		{
			int tmp = edges[i];
			edges[i] = edges[i-1] + cprev;
			cprev = tmp;
		}
		
		numControlPoints = edges[n_edges-1]+cprev;
	}	
}



__global__ void kernelRandomInit(curandState* state)					//Initialize one random number generator state
{
	int id = threadIdx.x;
	curand_init(1234,id,0,state+id);
}

extern "C" void random_init(curandState* d_states)						//Initialize NTHREADS random generators.
{																		//We'll use them later in kernels when we need random numbers.
	kernelRandomInit<<<1,NTHREADS>>>(d_states);
	cudaThreadSynchronize();
}


extern "C" void resample(BundlingPoint* new_points,int& n_outpoints,int* out_edges,BundlingPoint* in_points,int n_inpoints,int* in_edges,int* h_edges,int n_edges,float delta,curandState* d_states,float jitter,
						 float* d_edgeProfile)
{
	int threads    = int(NTHREADS);										//Prepare the resample kernel
	int numedges_b = iAlignUp(n_edges,threads);							//Find higher multiple of blocksize than # edges
	int blocks     = int(numedges_b/threads);
		
	cudaBindTexture(0,texSites,in_points);								//Bind 2D sites to a 1D texture
	cudaBindTexture(0,texStarts,in_edges);								//Bind edge-start offsets in above vector to another 1D texture

	kernelResampleCount<<<blocks,threads>>>(out_edges,n_edges,delta);	//1. Compute #points that resampling produces on each edge. Store this in out_edges[]
	cudaThreadSynchronize();	

	cudaMemcpy(h_edges,out_edges,n_edges*sizeof(int),cudaMemcpyDeviceToHost);
	int cprev = h_edges[0];												//2. From 1, compute start-offset of resampled edges. Knowing this allows us to parallelize
	h_edges[0] = 0;														//   the resampling and writing the resampled points (in pass 3 below)
	for(int i=1;i<n_edges;++i)											//REMARK: This is the only still required CPU-GPU communication for the bundling algorithm..
	{
		int tmp = h_edges[i];
		h_edges[i] = h_edges[i-1] + cprev;
		cprev = tmp;
	}
	int NP = h_edges[n_edges-1]+cprev;
	h_edges[n_edges] = NP;												//Put an extra item at end, equal to the #points
	
	cudaMemcpy(out_edges,h_edges,(n_edges+1)*sizeof(int),cudaMemcpyHostToDevice);
	cudaThreadSynchronize();
	
	cudaBindTexture(0,texEdges,out_edges);											//3. Resample edges, producing same #points/edge as in pass 1. Put results in
	cudaBindTexture(0,texEdgeProfile,d_edgeProfile);								//   contiguous vector 'new_points'	
	kernelResample<<<blocks,threads>>>(new_points,n_edges,delta,jitter,d_states);	
	cudaThreadSynchronize();

	cudaUnbindTexture(texEdgeProfile);
	cudaUnbindTexture(texEdges);	
	cudaUnbindTexture(texSites);
	cudaUnbindTexture(texStarts);
	
	n_outpoints = NP;	
}


